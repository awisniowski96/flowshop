﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1SPD
{
    class TabelaCzasu
    {
        int zadania;
        int maszyny;
        int[,] tabela;
        int[,] kopiaTabeli;

        public TabelaCzasu(int iloscZadan, int iloscMaszyn)
        {
            zadania = iloscZadan;
            maszyny = iloscMaszyn;
            tabela = new int[zadania, maszyny];
            kopiaTabeli = new int[zadania, maszyny];
        }

        public void WyswietlTabele()
        {
            for (int i = 0; i < zadania; i++)
            {
                for (int j = 0; j < maszyny; j++)
                {
                    Console.Write(tabela[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        public void WczytajWartosciZPliku(StreamReader plik)
        {
            string[] linijka;
            for (int i = 0; i < zadania; i++)
            {
                linijka = plik.ReadLine().Split(' ');
                for (int j = 0; j < maszyny; j++)
                {
                    tabela[i, j] = int.Parse(linijka[j]);
                    kopiaTabeli[i, j] = int.Parse(linijka[j]);
                }

            }

        }

        public void UszeregujCzas()
        {
            SortedList posortowanyCzas = new SortedList();
            double sumaCzasu;
            int licznik = zadania - 1;
            for (int i = 0; i < zadania; i++)
            {
                sumaCzasu = 0;
                for (int j = 0; j < maszyny; j++)
                {
                    sumaCzasu += tabela[i, j];
                }

                if (posortowanyCzas.ContainsKey(sumaCzasu))
                    sumaCzasu += 0.01;
                posortowanyCzas[sumaCzasu] = i;
            }

            foreach (DictionaryEntry zadanie in posortowanyCzas)
            {
                for (int i = 0; i < maszyny; i++)
                {
                    tabela[licznik, i] = kopiaTabeli[(int)zadanie.Value, i];
                }
                licznik--;
            }


        }

        public int ObliczCzas(int iloscZadan)
        {
            int[,] tablicaCzasu = new int[iloscZadan, maszyny];


            tablicaCzasu[0, 0] = tabela[0, 0];
            for (int i = 1; i < maszyny; i++)
            {
                tablicaCzasu[0, i] = tabela[0, i] + tablicaCzasu[0, i - 1];
            }
            for (int i = 1; i < iloscZadan; i++)
            {
                tablicaCzasu[i, 0] = tabela[i, 0] + tablicaCzasu[i - 1, 0];
            }

            for (int i = 1; i < iloscZadan; i++)
            {
                for (int j = 1; j < maszyny; j++)
                {
                    tablicaCzasu[i, j] = Math.Max(tablicaCzasu[i - 1, j], tablicaCzasu[i, j - 1]) + tabela[i, j];
                }
            }

            return tablicaCzasu[iloscZadan - 1, maszyny - 1];
        }

        private void ZamienWiersz(int pierwszy, int drugi)
        {
            int[] bufor = new int[maszyny];
            for (int i = 0; i < maszyny; i++)
            {
                bufor[i] = tabela[pierwszy, i];
                tabela[pierwszy, i] = tabela[drugi, i];
                tabela[drugi, i] = bufor[i];
            }
        }

        public int ObliczOptymalnyCzas()
        {
            int czas;
            int temp;
            int pozycja;

            czas = ObliczCzas(2);
            ZamienWiersz(0, 1);
            if ((temp = ObliczCzas(2)) < czas)
                czas = temp;
            else
                ZamienWiersz(0, 1);

            for (int i = 2; i < zadania; i++)
            {
                pozycja = i;

                czas = ObliczCzas(i + 1);


                for (int j = 0; j < i; j++)
                {
                    ZamienWiersz(i - j, i - j - 1);
                    if ((temp = ObliczCzas(i + 1)) < czas)
                    {
                        pozycja = i - j - 1;
                        czas = temp;
                    }
                }

                for (int g = 0; g < pozycja; g++)
                {
                    ZamienWiersz(g, g + 1);
                }

            }

            
            return czas;
        }
    }
}
