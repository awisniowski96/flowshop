﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1SPD
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader plik = new StreamReader(@"D:\Users\Olek\source\repos\lab1SPD\NEH7.DAT");
            string[] dane = plik.ReadLine().Split(' ');
            TabelaCzasu tabelka = new TabelaCzasu(int.Parse(dane[0]), int.Parse(dane[1]));
            tabelka.WczytajWartosciZPliku(plik);
            tabelka.WyswietlTabele();
            tabelka.UszeregujCzas();
            Console.WriteLine();
            tabelka.WyswietlTabele();
            Console.WriteLine(tabelka.ObliczOptymalnyCzas());

        }
    }
}
